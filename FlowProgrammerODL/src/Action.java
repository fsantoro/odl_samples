
public class Action {

	private String action;
	private String value;
	
	public Action(String _action, String _value){
		this.action = _action;
		this.value = _value;
	}

	public String getAction() {
		return action;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String toString(){
	
		return action+"="+value;
	}
	
}
