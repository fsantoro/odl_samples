import java.util.Collection;
import java.util.LinkedList;


public class FlowConfig {

	private String tosBits;
	private String ingressPort;
	private String name;
	private String idleTimeout;
	private String vlanPriority;
	private String tpDst;
	private Node node;
	private String cookie;
	private String nwSrc;
	private String protocol;
	private String etherType;
	private String dlDst;
	private String installInHw;
	private String priority;
	private String nwDst;
	private String vlanId;
	private String dlSrc;
	private String tpSrc;
	private String hardTimeout;
	private Collection<String> actions;
	
	public FlowConfig(){
		
		actions = new LinkedList<String>();
		
	}
	
	public void setVlanPriority(String vlanPriority) {
		this.vlanPriority = vlanPriority;
	}
	
	public void setVlanId(String vlanId) {
		this.vlanId = vlanId;
	}
	
	public void setTpSrc(String tpSrc) {
		this.tpSrc = tpSrc;
	}
	
	public void setTpDst(String tpDst) {
		this.tpDst = tpDst;
	}
	
	public void setTosBits(String tosBits) {
		this.tosBits = tosBits;
	}
	
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	public void setNwSrc(String nwSrc) {
		this.nwSrc = nwSrc;
	}
	
	public void setNwDst(String nwDst) {
		this.nwDst = nwDst;
	}
	
	public void setNode(Node node) {
		this.node = node;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setInstallInHw(String installInHw) {
		this.installInHw = installInHw;
	}
	
	public void setIngressPort(String ingressPort) {
		this.ingressPort = ingressPort;
	}
	
	public void setIdleTimeout(String idleTimeout) {
		this.idleTimeout = idleTimeout;
	}
	
	public void setHardTimeout(String hardTimeout) {
		this.hardTimeout = hardTimeout;
	}
	
	public void setEtherType(String etherType) {
		this.etherType = etherType;
	}
	
	public void setDlSrc(String dlSrc) {
		this.dlSrc = dlSrc;
	}
	
	public void setDlDst(String dlDst) {
		this.dlDst = dlDst;
	}
	
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	
	public String getVlanPriority() {
		return vlanPriority;
	}
	
	public String getVlanId() {
		return vlanId;
	}
	
	public String getTpSrc() {
		return tpSrc;
	}
	
	public String getTpDst() {
		return tpDst;
	}
	
	public String getTosBits() {
		return tosBits;
	}
	
	public String getProtocol() {
		return protocol;
	}
	
	public String getPriority() {
		return priority;
	}
	
	public String getNwSrc() {
		return nwSrc;
	}
	
	public String getNwDst() {
		return nwDst;
	}
	
	public Node getNode() {
		return node;
	}
	
	public String getName() {
		return name;
	}
	
	public String getInstallInHw() {
		return installInHw;
	}
	
	public String getIngressPort() {
		return ingressPort;
	}
	
	public String getIdleTimeout() {
		return idleTimeout;
	}
	
	public String getHardTimeout() {
		return hardTimeout;
	}
	
	public String getEtherType() {
		return etherType;
	}
	
	public String getDlSrc() {
		return dlSrc;
	}
	
	public String getDlDst() {
		return dlDst;
	}
	
	public String getCookie() {
		return cookie;
	}
	
	public void addAction(String a){
		actions.add(a);	
	}
	
	public void removeAction(String a){
		actions.remove(a);
	}
	
	public void removeAllActions(){
		actions.removeAll(actions);
	}
	
	
}
