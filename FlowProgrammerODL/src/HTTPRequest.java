import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class HTTPRequest {

	private CloseableHttpClient httpClient;
	private HttpPut httpPut;
	
	public HTTPRequest(String url, String username, String password){
		
		httpClient = HttpClients.createDefault();
		httpPut = new HttpPut(url);
		
		String basic_auth = new String(Base64.encodeBase64((username + ":" + password).getBytes()));
		
		httpPut.addHeader("Authorization", "Basic "+basic_auth);
		httpPut.addHeader("Content-type", "application/json");
		
		
		
	}
	
	public int send(FlowConfig fc){
		
		GsonBuilder builder = new GsonBuilder();
		builder.disableHtmlEscaping();
		
		Gson gson = builder.create();
		
		String s = gson.toJson(fc);
		
		HttpEntity entity;
		try {
			entity = new StringEntity(s);
			httpPut.setEntity(entity);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			HttpResponse res = httpClient.execute(httpPut);
			return res.getStatusLine().getStatusCode();
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 1;
	}
	
}
