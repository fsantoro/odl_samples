
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String s = "http://localhost:8080/controller/nb/v2/flowprogrammer/default/node/OF/00:00:00:00:00:00:00:01/staticFlow/";
		
		
		HTTPRequest rq = new HTTPRequest(s+"flow1","admin","admin");
		FlowConfig fc = new FlowConfig();
		
		fc.setInstallInHw("true");
		fc.setName("flow1");
		fc.setNode(new Node("00:00:00:00:00:00:00:01","OF"));
		fc.setIngressPort("1");
		fc.setEtherType("0x800");
		//fc.setNwDst("10.0.0.4");		
		Action a = new Action("OUTPUT","2");
		//Action b = new Action("SET NW_SRC","10.0.0.44");
		fc.addAction(a.toString());
		//fc.addAction(b.toString());
		
		System.out.println(rq.send(fc));
		
		HTTPRequest rq2 = new HTTPRequest(s+"flow2","admin","admin");
		FlowConfig fc2 = new FlowConfig();
		fc2.setInstallInHw("true");
		fc2.setName("flow2");
		fc2.setNode(new Node("00:00:00:00:00:00:00:01","OF"));
		fc2.setIngressPort("2");
		fc2.setEtherType("0x800");
		//fc2.setNwDst("10.0.0.2");		
		Action a2 = new Action("OUTPUT","1");
		fc2.addAction(a2.toString());
		
		System.out.println(rq2.send(fc2));
		
		
		
	}

}
