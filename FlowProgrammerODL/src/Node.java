
public class Node {

	private String id;
	private String type;
	
	public Node(String _id, String _type){
		
		this.id = _id;
		this.type = _type;
		
	}
	
	public String getId(){
		return id;
	}
	
	public String getType(){
		return type;
	}
	
	public void setId(String _id){
		this.id = _id;
	}
	
	public void setType(String _type){
		this.type = _type;
	}
}
