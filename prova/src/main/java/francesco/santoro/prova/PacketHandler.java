package francesco.santoro.prova;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;

import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.flowprogrammer.Flow;
import org.opendaylight.controller.sal.flowprogrammer.IFlowProgrammerService;
import org.opendaylight.controller.sal.match.Match;
import org.opendaylight.controller.sal.match.MatchType;
import org.opendaylight.controller.sal.packet.Ethernet;
import org.opendaylight.controller.sal.packet.IDataPacketService;
import org.opendaylight.controller.sal.packet.IListenDataPacket;
import org.opendaylight.controller.sal.packet.IPv4;
import org.opendaylight.controller.sal.packet.Packet;
import org.opendaylight.controller.sal.packet.PacketResult;
import org.opendaylight.controller.sal.packet.RawPacket;
import org.opendaylight.controller.sal.utils.Status;
import org.opendaylight.controller.switchmanager.ISwitchManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.opendaylight.controller.sal.action.*;

public class PacketHandler implements IListenDataPacket {
 
    private static final Logger log = LoggerFactory.getLogger(PacketHandler.class);
    private IDataPacketService dataPacketService;
    private IFlowProgrammerService flowProgrammerService;
    private ISwitchManager switchManagerService;
 
    static private InetAddress intToInetAddress(int i) {
        byte b[] = new byte[] { (byte) ((i>>24)&0xff), (byte) ((i>>16)&0xff), (byte) ((i>>8)&0xff), (byte) (i&0xff) };
        InetAddress addr;
        try {
            addr = InetAddress.getByAddress(b);
        } catch (UnknownHostException e) {
            return null;
        }
 
        return addr;
    }
 
    /*
     * Sets a reference to the requested DataPacketService
     * See Activator.configureInstance(...):
     * c.add(createContainerServiceDependency(containerName).setService(
     * IDataPacketService.class).setCallbacks(
     * "setDataPacketService", "unsetDataPacketService")
     * .setRequired(true));
     */
    void setDataPacketService(IDataPacketService s) {
        log.trace("Set DataPacketService.");
 
        dataPacketService = s;
    }
 
    /*
     * Unsets DataPacketService
     * See Activator.configureInstance(...):
     * c.add(createContainerServiceDependency(containerName).setService(
     * IDataPacketService.class).setCallbacks(
     * "setDataPacketService", "unsetDataPacketService")
     * .setRequired(true));
     */
    void unsetDataPacketService(IDataPacketService s) {
        log.trace("Removed DataPacketService.");
 
        if (dataPacketService == s) {
            dataPacketService = null;
        }
    }
    
    void setFlowProgrammerService(IFlowProgrammerService s){
    	log.trace("Set FlowProgrammerService");
    	
    	this.flowProgrammerService = s;
    }
    
    void unsetFlowProgrammerService(IFlowProgrammerService s){
    	log.trace("Uset FlowProgrammerService");
    	
    	if(flowProgrammerService == s){
    		flowProgrammerService = null;
    	}
    }
 
    void setSwitchManagerService(ISwitchManager s){
    	log.trace("Set SwitchManagerService");
    	
    	switchManagerService = s;
    }
    
    void unsetSwitchManagerService(ISwitchManager s){
    	log.trace("Unset SwitchManagerService");
    	
    	if (switchManagerService == s){
    		switchManagerService = null;
    	}
    }
    
    public PacketResult receiveDataPacket(RawPacket inPkt) {
        log.trace("Received data packet.");
 
        // The connector, the packet came from ("port")
        NodeConnector ingressConnector = inPkt.getIncomingNodeConnector();
        // The node that received the packet ("switch")
        Node node = ingressConnector.getNode();
 
        // Use DataPacketService to decode the packet.
        Packet l2pkt = dataPacketService.decodeDataPacket(inPkt);
 
        if (l2pkt instanceof Ethernet) {
            Object l3Pkt = l2pkt.getPayload();
            if (l3Pkt instanceof IPv4) {
                IPv4 ipv4Pkt = (IPv4) l3Pkt;
                int dstAddr = ipv4Pkt.getDestinationAddress();
                InetAddress addr = intToInetAddress(dstAddr);
                
                System.out.println("NO MATCH");
                System.out.println("Pkt. to " + addr.toString() + " received by node " + node.getNodeIDString() + " on connector " + ingressConnector.getNodeConnectorIDString());
                
                Match m = new Match();  
                LinkedList<Action> actions = new LinkedList<Action>();
                
                m.setField(MatchType.IN_PORT, ingressConnector);
                //m.setField(MatchType., value);
                
                NodeConnector egressConnector;
				if (ingressConnector.getNodeConnectorIDString().compareTo("1") == 0){
                
                	egressConnector = switchManagerService.getNodeConnector(node, "s1-eth2");
                	actions.add(new Output(egressConnector));
                	
                	//System.out.println("OK if"+egressConnector.getNodeConnectorIdAsString());
                	
                }
                else{
                	egressConnector = switchManagerService.getNodeConnector(node, "s1-eth1");
                	actions.add(new Output(egressConnector));
                	
                	//System.out.println(egressConnector.getNodeConnectorIdAsString());
                	
                }
                Flow f = new Flow(m,actions);
                
                Status status = flowProgrammerService.addFlow(node, f);
                
                if (!status.isSuccess()){
                	System.out.println("Error");
                	return PacketResult.CONSUME;
                }
                
                else {
                	System.out.println("OK");
                	
                	inPkt.setOutgoingNodeConnector(egressConnector);
                	dataPacketService.transmitDataPacket(inPkt);
                	
                	return PacketResult.KEEP_PROCESSING;
                }
            }
        }
        // We did not process the packet -> let someone else do the job.
        return PacketResult.IGNORED;
    }
}